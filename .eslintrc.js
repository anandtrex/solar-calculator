module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    
    "extends": ["plugin:vue/recommended", "eslint:recommended"],
    "parserOptions": {
        "sourceType": "module",
        "parser": "babel-eslint",
    },
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "never"
        ],
        "no-inner-declarations": "off",
        "no-console": 0,
        "no-unused-vars": 0
    }
}