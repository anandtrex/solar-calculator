module.exports = {
    baseUrl: process.env.NODE_ENV === "production"
        ? "https://solar-path.anand.ink/"
        : "/"
}
